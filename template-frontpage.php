<?php
/**
 *Template Name: Frontpage
 *
 * @package OnePress
 */

get_header(); ?>
	<div id="content" class="site-content">
		<main id="main" class="site-main" role="main">			
            <?php

            do_action( 'onepress_frontpage_before_section_parts' );

			if ( ! has_action( 'onepress_frontpage_section_parts' ) ) {

				$sections = apply_filters( 'onepress_frontpage_sections_order', array(
                    'features', 'about', 'services', 'videolightbox', 'gallery', 'counter', 'team',  'news', 'contact'
                ) );

				foreach ( $sections as $section ){
                    onepress_load_section( $section );
				}

			} else {
				do_action( 'onepress_frontpage_section_parts' );
			}

            do_action( 'onepress_frontpage_after_section_parts' );

			?>
			<center><h1>Аренда спецтехники</h1></center>
<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Ac8f45497af29881c8d5a2bfb9c3ab6b7e4f7da8b9e17f42bb014714b16f38109&amp;source=constructor" width="100%" height="434" frameborder="0"></iframe>
		</main><!-- #main -->
	</div><!-- #content -->

<?php get_footer(); ?>
